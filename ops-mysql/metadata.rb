name             'ops-mysql'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures ops-mysql'
long_description 'Installs/Configures ops-mysql'
version          '0.1.0'

depends       'mysql', '~> 6.0'