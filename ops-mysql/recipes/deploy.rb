node[:deploy].each do |application, deploy|
  if application == "wordpress"
    opsworks_deploy_dir do
      user deploy[:user]
      group deploy[:group]
      path deploy[:deploy_to]
    end
    opsworks_deploy do
      deploy_data deploy
      app application
    end

  
    directory '/var/www/html' do
      owner 'root'
      group 'root'
      mode '0755'
      recursive true
      action :delete
    end 

    link '/var/www/html' do
      to "#{node[:deploy][:revive][:deploy_to]}/current"
      not_if 'test -L /var/www/html'
    end

  end
end