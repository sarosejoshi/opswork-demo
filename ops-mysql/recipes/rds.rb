package 'mysql-devel'
chef_gem 'mysql2'

#backup_sql = '/path/to/the/db-backup.sql'

cookbook_file '/tmp/immersv_revive_install.sql' do
  mode '0644'
  source "immersv_revive_install.sql"
  action :create
end

mysql_connection_info = {
  :host => node[:deploy]['revive'][:environment_variables][:RDSHost],
  :username => node[:deploy]['revive'][:environment_variables][:RDSUser],
  :password => node[:deploy]['revive'][:environment_variables][:RDSPass]
}
mysql_database 'revive' do
  connection mysql_connection_info
  action [:drop]
end

mysql_database 'revive' do
  connection mysql_connection_info
  action [:create]
end

execute 'import-sql' do
  command "mysql -h #{node[:deploy]['revive'][:environment_variables][:RDSHost]} -u #{node[:deploy]['revive'][:environment_variables][:RDSUser]} -p'#{node[:deploy]['revive'][:environment_variables][:RDSPass]}' revive < /tmp/immersv_revive_install.sql"
end