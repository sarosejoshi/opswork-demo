#
# Cookbook Name:: ops-vpn
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
file '/etc/redhat-release' do
  content 'Red Hat'
end

remote_file "/tmp/openvpn-as-2.0.21-CentOS6.x86_64.rpm" do
	source "http://swupdate.openvpn.org/as/openvpn-as-2.0.21-CentOS6.x86_64.rpm"
	action :create
end
rpm_package "openvpn-as" do
	source "/tmp/openvpn-as-2.0.21-CentOS6.x86_64.rpm"
	action :install
	not_if 'rpm -qa|grep openvpn-as-2.0.21-CentOS6'
end

file '/etc/redhat-release' do
  action :delete
end
service 'openvpnas' do
   action [:start,:enable]
end
#bash 'enable google auth' do
#  user 'root'
 # cwd "/usr/local/openvpn_as/"
#  code <<-EOH
 # /usr/local/openvpn_as/scripts/sacli --key vpn.server.google_auth.disable --value true ConfigPut
#  /usr/local/openvpn_as/scripts/sacli start
#  EOH
#end