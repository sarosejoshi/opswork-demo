extract_path = "/var/www/html"

 user 'apache' do
 	ignore_failure true
 	group "apache"
 end

 %w{ httpd24 httpd24-devel php56 php-pear-MDB2-Driver-mysql php56-xml php56-gd php56-mbstring php56-pecl-apcu php56-mysqlnd  }.each do |pkg|
   package pkg
end

service 'httpd' do
   action :enable
end

template '/etc/php.ini' do
  source 'php.ini.erb'
  mode '0755'
  owner 'root'
  group 'root'
  notifies :restart, "service[httpd]"
end

remote_file '/tmp/latest.tar.gz' do
  source 'https://wordpress.org/latest.tar.gz'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

bash 'extract_module' do
  code <<-EOH
    mkdir -p /var/www/html
    cd /var/www/html/
    tar -zxf /tmp/latest.tar.gz 
    chown -R root:root /var/www/html 
    EOH
  only_if { ::File.exists?('/tmp/latest.tar.gz') }
end

template '/etc/httpd/conf/httpd.conf' do
  source 'httpd.conf.erb'
  mode '0755'
  owner 'root'
  group 'root'
  notifies :restart, "service[httpd]"
end
