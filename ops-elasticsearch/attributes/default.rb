default['elasticsearch']['config']['cluster.name'] = 'immersv'
override['elasticsearch']['data_dir']= '/ebs/elasticsearch'
default['elasticsearch']['config']['network.bind_host'] = '0.0.0.0'
default['elasticsearch']['version'] = '2.1.1'
default['elasticsearch']['config']['node.name'] = node["opsworks"]["instance"]["hostname"]