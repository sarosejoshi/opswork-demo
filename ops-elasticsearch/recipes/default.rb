#
# Cookbook Name:: ops-elasticsearch
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'ops-base'
include_recipe 'ops-base::java'

stack = node[:opsworks][:stack][:name]
#elastic_instances = Array.new
#    node["opsworks"]["layers"]["#{stack}-elasticsearch"]["instances"].map do |instance_name, instance_config|
#        elastic_instances << instance_config['private_ip']
#    end

#node.default['elasticsearch']['config']['discovery.zen.ping.unicast.hosts'] = elastic_instances

#instances = node["opsworks"]["layers"]["#{stack}-elasticsearch"]["instances"].size;
#instances = (instances / 2.0).ceil
#node.default['elasticsearch']['config']['discovery.zen.minimum_master_nodes'] = instances

directory '/etc/elasticsearch' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

directory '/etc/elasticsearch/templates' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end
%w( adtemplate.json analyticstemplate.json ).each do |tmp|
  template "/etc/elasticsearch/templates/#{tmp}" do
    source "#{tmp}.erb"
    owner 'root'
    group 'root'
    mode '0755'
  end
end
include_recipe 'elasticsearch-cluster'