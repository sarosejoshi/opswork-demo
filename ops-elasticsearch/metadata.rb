name             'ops-elasticsearch'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures ops-elasticsearch'
long_description 'Installs/Configures ops-elasticsearch'
version          '0.1.0'


depends 'ops-base'
depends 'elasticsearch-cluster'