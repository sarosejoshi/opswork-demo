#
# Cookbook Name:: ops-dashboard
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#


include_recipe 'ops-base'
include_recipe 'ops-base::java'

include_recipe 'wildfly::install'

service 'wildfly' do
  action [:enable, :restart]
end
