node[:deploy].each do |application, deploy| 
  if application == "javaapp"
  stack_name = node["opsworks"]["stack"]["name"]
 
  
  directory "/etc/dashboard" do
    mode 0755
    recursive true
    action :create
  end
  
 
  template '/etc/dashboard/jpa.config' do
    source 'jpa.config.erb'
    mode '0644'
  end
  template '/etc/dashboard/global.config' do
    source 'global.config.erb'
    mode '0644'
  end
  template '/etc/dashboard/mail.config' do
    source 'mail.config.erb'
    mode '0644'
  end
  template '/etc/dashboard/s3.config' do
    source 's3.config.erb'
    mode '0644'
  end
    bash "deploy dashboard" do
      user "root"
      code <<-EOH
      cd /opt
      aws s3 --region us-west-2 cp   s3://jdpa-app-archive/sample.war  dashboard/ROOT.war
      /opt/wildfly/bin/jboss-cli.sh --connect --command="deploy --force dashboard/ROOT.war"
      EOH
    end
  end
  
  service 'wildfly' do
    action [:enable, :start]
  end
  
end