package 'mysql-devel'
chef_gem 'mysql2'

cookbook_file '/tmp/ImmersvSystemData.sql' do
  mode '0644'
  source "ImmersvSystemData.sql"
  action :create
end

cookbook_file '/tmp/init_systemdata.sql' do
  mode '0644'
  source "init_systemdata.sql"
  action :create
end

mysql_connection_info = {
  :host => node[:deploy]['dashboard'][:environment_variables][:RDSHost],
  :username => node[:deploy]['dashboard'][:environment_variables][:RDSUser],
  :password => node[:deploy]['dashboard'][:environment_variables][:RDSPass]
}

mysql_database 'systemdata' do
  connection mysql_connection_info
  action [:drop]
end
mysql_database 'systemdata' do
  connection mysql_connection_info
  action [:create]
end

execute 'import-schema' do
  command "mysql -h #{node[:deploy]['dashboard'][:environment_variables][:RDSHost]} -u #{node[:deploy]['dashboard'][:environment_variables][:RDSUser]} -p'#{node[:deploy]['dashboard'][:environment_variables][:RDSPass]}' systemdata < /tmp/ImmersvSystemData.sql"
end

execute 'import-init_data' do
  command "mysql -h #{node[:deploy]['dashboard'][:environment_variables][:RDSHost]} -u #{node[:deploy]['dashboard'][:environment_variables][:RDSUser]} -p'#{node[:deploy]['dashboard'][:environment_variables][:RDSPass]}' systemdata < /tmp/init_systemdata.sql"
end