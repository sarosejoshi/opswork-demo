#
# Cookbook Name:: ops-kibana
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe "ops-base"
package 'nginx'

#stack_name = node["opsworks"]["stack"]["name"]
#hostname="#{node[:opsworks][:stack][:name]}-zookeeper1"

template "/etc/nginx/htpasswd.users" do
  source 'htpasswd.users.erb'
  mode '0644'
  notifies :restart, "service[nginx]", :immediately
end

template "/etc/nginx/nginx.conf" do
  source "nginx.conf.erb"
  notifies :restart, "service[nginx]", :immediately
end

user 'kibana' do
  shell '/bin/bash'
end

template '/etc/init.d/kibana' do
  source 'kibana.init.erb'
  mode '0755'
end

template '/etc/default/kibana' do
  source 'kibana.default.erb'
  mode '0644'
end


remote_file "/opt/kibana-4.3.0-linux-x64.tar.gz" do
 source "https://download.elastic.co/kibana/kibana/kibana-4.3.0-linux-x64.tar.gz"
end
bash "install kibana" do
  user "root"
  cwd "/opt"
  code <<-EOH
  cd /opt
  tar -xf kibana-4.3.0-linux-x64.tar.gz
  ln -s kibana-4.3.0-linux-x64 kibana
  chown -R kibana:kibana kibana-4.3.0-linux-x64
  EOH
  not_if "test -f /opt/kibana/bin/kibana"
end


%w[ /opt/kibana /opt/kibana/config ].each do |path|
    directory path do
        mode 0755
        owner 'root'
        group 'root'
        action :create
    end
end

template "/opt/kibana/config/kibana.yml" do
    source "kibana.yml.erb"
    mode 00755
    variables(
              :elasticsearch_ip => "es1-elasticsearch"
              )
end

bash "start kibana" do
    user "root"
    code <<-EOH
    service kibana start
    sleep 1
    service nginx restart
    EOH
end

service "nginx" do
  action [:enable, :start]
end
service "kibana" do
  action [:enable, :start]
end